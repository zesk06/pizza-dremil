# pizza-dremil

Le site non-officiel de Pizz'a Drémil, la pizzeria de Drémil Lafage

- publication: https://zesk06.gitlab.io/pizza-dremil/ 
- site officiel: https://www.pizzeria-dremil.fr/

## developpement

- install [yarn](https://yarnpkg.com/)
  ``npm install -g yarn``
- install pizza-dremil
  ``make init``
- serve
  ``make serve``



