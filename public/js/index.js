
Vue.component('pizza-item', {
  props: ['pizza'],
  template: '<div class="menu-text"> <div class="menu-text-left"> <h4>{{ pizza.aname }}</h4> <h6>{{ pizza.ingredients}}</h6> </div> <div class="menu-text-midle"> <span class="line"> </span> </div> <div class="menu-text-right"> <h4>{{ pizza.prix_p }}/ {{pizza.prix_g }}</h4> </div> <div class="clearfix"></div></div>'
})

var app = new Vue({ 
    el: '#app',
    data: {
        pizzaList: [],
      message: "coucou"
    },
  mounted(){
    var self = this;
    $.getJSON("./pizzas.json", json => {
      self.pizzaList = json.pizzas;
      console.log("loaded ", self.pizzaList);
    });
  }
});
